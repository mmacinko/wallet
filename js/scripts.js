$(function() {
    $('#sidebar').css('min-height', $('.row-offcanvas').outerHeight());

    $('[data-toggle="tooltip"]').tooltip();
    $('#side-menu').metisMenu();

    $('.filter').on('click', '.btn', function(e){
        $(e.currentTarget).toggleClass('active').blur();
        $('.filters').slideToggle('fast');
    });
    $('.btn-filter').on('click', function(e){
        $(e.currentTarget).toggleClass('active').blur();
    });

    $('[data-toggle="offcanvas"]').click(function () {
        console.log('bla');
        $('.row-offcanvas').toggleClass('active')
    });

    $('.formats').on('click', 'a', function(e){
        e.preventDefault();
        $('.formats a').removeClass('active');
        $(this).addClass('active');
    });

    $('#filter_from').datetimepicker();
    $('#filter_till').datetimepicker();
    $("#filter_from").on("dp.change", function (e) {
        $('#filter_till').data("DateTimePicker").minDate(e.date);
    });
    $("#filter_till").on("dp.change", function (e) {
        $('#filter_from').data("DateTimePicker").maxDate(e.date);
    });

    $('.table th input[type=checkbox]').on('click', function(e){
        $('.table th input[type=checkbox]').toggleClass('active').blur();
    });
    $('.table th input[type=checkbox]').on('click', function(e) { 
        if(this.checked) {
            $('.table td input[type=checkbox]').each(function() {
                this.checked = true;            
            });
        }else{
            $('.table td input[type=checkbox]').each(function() {
                this.checked = false;                     
            });         
        }
    });
    
    /* Formatting function for row details - modify as you need */
    function transactions_format ( name, value ) {
        return '<table class="table intable">'+
            '<thead>'+
                '<tr>'+
                    '<th colspan="2">Session Details</th>'+
                    '<th colspan="3">Point Of Sale Details</th>'+
                    '<th colspan="3">Items</th>'+
                '</tr>'+
            '</thead>'+
            '<tr>'+
                '<td colspan="2">'+
                    '<table class="table">'+
                        '<tr>'+
                            '<td>Transaction ID<td>'+
                            '<td>23325423228490<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Receipt ID<td>'+
                            '<td>43324592004900<td>'+
                        '</tr>'+
                    '</table>'+
                '</td>'+
                '<td colspan="3">'+
                    '<table class="table">'+
                        '<tr>'+
                            '<td>Location<td>'+
                            '<td>NDC Head Office<td>'+
                            '<td>(ID: 987)<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Point of Sale<td>'+
                            '<td>Wurlitzer 1000<td>'+
                            '<td>(ID: 232)<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Terminal ID<td>'+
                            '<td>1409020113<td>'+
                            '<td><td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>POS Saldo<td>'+
                            '<td>12,00 €<td>'+
                            '<td><td>'+
                        '</tr>'+
                    '</table>'+
                '</td>'+
                '<td colspan="3">'+
                    '<table class="table">'+
                        '<tr>'+
                            '<td>1.<td>'+
                            '<td>Coca Cola 0.5<td>'+
                            '<td>1,50 €<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>2.<td>'+
                            '<td>Coca Cola 0.5<td>'+
                            '<td>1,50 €<td>'+
                        '</tr>'+
                    '</table>'+
                '</td>'+
            '</tr>'+
        '</table>';
    }

    /* Formatting function for row details - modify as you need */
    function activities_monitoring_format ( name, value ) {
        return '<table class="table intable">'+
            '<thead>'+
                '<tr>'+
                    '<th colspan="2">Input info</th>'+
                    '<th colspan="3">Activity results</th>'+
                    '<th colspan="3">Loyalty transaction</th>'+
                '</tr>'+
            '</thead>'+
            '<tr>'+
                '<td colspan="2">'+
                    '<table class="table">'+
                        '<tr>'+
                            '<td>User reporting<td>'+
                            '<td>Broker<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>User role<td>'+
                            '<td>Superadmin<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>External activity<td>'+
                            '<td>No<td>'+
                        '</tr>'+
                    '</table>'+
                '</td>'+
                '<td colspan="3">'+
                    '<table class="table">'+
                        '<tr>'+
                            '<td>ID<td>'+
                            '<td>392948230<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Type<td>'+
                            '<td>Payment Cashless<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>POS<td>'+
                            '<td>Bolnica: Cashless Vending Machine<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Amount<td>'+
                            '<td>34 €<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Items<td>'+
                            '<td colspan="2">'+
                                '<table class="table">'+
                                    '<tr>'+
                                        '<td>30€<td>'+
                                        '<td>Cheesburger<td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>4€<td>'+
                                        '<td>Fanta<td>'+
                                    '</tr>'+
                                '</table>'+
                            '<td>'+
                        '</tr>'+
                    '</table>'+
                '</td>'+
                '<td colspan="3">'+
                    '<table class="table">'+
                        '<tr>'+
                            '<td>1.<td>'+
                            '<td>Coca Cola 0.5<td>'+
                            '<td>1,50 €<td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>2.<td>'+
                            '<td>Coca Cola 0.5<td>'+
                            '<td>1,50 €<td>'+
                        '</tr>'+
                    '</table>'+
                '</td>'+
            '</tr>'+
        '</table>';
    }

    $(document).ready(function() {
        $('#dataTable-accounts').DataTable({
            responsive: true,
            //lengthChange: false,
            filter: false,
            info: false,
            dom: "<'row'<'col-sm-6'f><'col-sm-6'i>>" +
                 "<'row'<'col-sm-12'tr>>" +
                 "<'row'<'col-sm-6'p><'col-sm-6'l>>",
            "language": {
                "paginate": {
                    "previous": "«",
                    "next": "»"
                }
            },
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ]
        });

        var table_transactions = $('#dataTable-transactions').DataTable( {
            responsive: true,
            lengthChange: false,
            filter: false,
            info: false,
            dom: "<'row'<'col-sm-6'f><'col-sm-6'il>>" +
                 "<'row'<'col-sm-12'tr>>" +
                 "<'row'<'col-sm-12'p>>",
            "language": {
                "paginate": {
                    "previous": "«",
                    "next": "»"
                }
            }
        } );

        var table_activities_monitoring = $('#dataTable-activities-monitoring').DataTable( {
            responsive: true,
            lengthChange: false,
            filter: false,
            info: false,
            dom: "<'row'<'col-sm-6'f><'col-sm-6'il>>" +
                 "<'row'<'col-sm-12'tr>>" +
                 "<'row'<'col-sm-12'p>>",
            "language": {
                "paginate": {
                    "previous": "«",
                    "next": "»"
                }
            }
        } );
        // Add event listener for opening and closing details
        $('#dataTable-transactions tbody').on('click', 'a.details-control', function (e) {
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = table_transactions.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                $(this).text('Details');
            }
            else {
                // Open this row
                row.child( transactions_format( tr.data('child-name'), tr.data('child-value') ) ).show();
                tr.addClass('shown');
                $(this).text('Hide details');
            }
        } );

        // Add event listener for opening and closing details
        $('#dataTable-activities-monitoring tbody').on('click', 'a.details-control', function (e) {
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = table_activities_monitoring.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                $(this).text('Details');
            }
            else {
                // Open this row
                row.child( activities_monitoring_format( tr.data('child-name'), tr.data('child-value') ) ).show();
                tr.addClass('shown');
                $(this).text('Hide details');
            }
        } );

        $('#transaction_analysis_recharge').highcharts({
            title: {
                text: 'Recharge',
                x: -20 //center
            },
            xAxis: {
                categories: ['1', '2', '3', '4', '5', '6', '7']
            },
            yAxis: {
                title: {
                    text: null
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Tokyo',
                data: [10, 0, 0, 5, 5, 0, 10]
            }]
        });


        $('#new_card').on('click', function(e){
            e.preventDefault();

            $('#new_card_row').show();

            $(e.currentTarget).blur();
        });

    });

});



//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        $('#sidebar').css('min-height', $('.row-offcanvas').outerHeight());


        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});